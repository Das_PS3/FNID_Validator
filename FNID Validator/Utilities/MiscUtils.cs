﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Extra.Utilities
{
    internal static class MiscUtils
    {
        internal static void DeleteEmptyDirectories(string directory)
        {
            foreach (var dir in Directory.EnumerateDirectories(directory))
            {
                DeleteEmptyDirectories(dir);

                if (!Directory.EnumerateFileSystemEntries(dir).Any())
                    Directory.Delete(dir);
            }
        }

        internal static byte[] HexStringToByteArray(string hexString)
        {
            return Enumerable.Range(0, hexString.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hexString.Substring(x, 2), 16))
                             .ToArray();
        }

        internal static byte[] InitializeArray(this byte[] array, byte defaultValue)
        {
            for (int i = 0; i < array.Length; i++)
                array[i] = defaultValue;

            return array;
        }

        internal static byte[] HexStringToByteArray(object converted)
        {
            throw new NotImplementedException();
        }

        internal static bool IsBinaryFile(this string value, int sampleSize = 10240)
        {
            if (!File.Exists(value))
                throw new ArgumentException();

            var buffer = new char[sampleSize];
            string sampleContent;

            using (var sr = new StreamReader(value))
            {
                int length = sr.Read(buffer, 0, sampleSize);
                sampleContent = new string(buffer, 0, length);
            }

            //Look for 3 consecutive binary zeroes
            if (sampleContent.Contains("\0\0\0"))
                return true;

            return false;
        }

        internal static double PrintProgress(ulong progress, ulong size, double previousPercent, string file)
        {
            file = Path.GetFileNameWithoutExtension(file);

            double percent = Math.Round((progress * 100) / (double)size, 1);

            if (percent > previousPercent)
            {
                previousPercent = percent;
                string bar = new string('=', (int)(percent / 4));

                Console.CursorLeft = 0;
                Console.Write("{0} [{1,-25}] {2:0.0}%", file, bar, percent);
            }

            return previousPercent;
        }

        internal static int RoundUp(int numToRound, int multiple)
        {
            if (multiple == 0)
                return numToRound;

            int remainder = numToRound % multiple;

            if (remainder == 0)
                return numToRound;

            return numToRound + multiple - remainder;
        }

        internal static long RoundUp(long numToRound, int multiple)
        {
            if (multiple == 0)
                return numToRound;

            long remainder = numToRound % multiple;

            if (remainder == 0)
                return numToRound;

            return numToRound + multiple - remainder;
        }

        internal static string StringWrapper(string str, int columnWidth)
        {
            string[] words = str.Split(' ');

            var newSentence = new StringBuilder();

            string line = "";

            foreach (string word in words)
            {
                if ((line + word).Length > columnWidth)
                {
                    newSentence.AppendLine(line);

                    line = "";
                }

                line += string.Format("{0} ", word);
            }

            if (line.Length > 0)
                newSentence.AppendLine(line);

            return newSentence.ToString();
        }

        internal static string StripHTML(string HTMLText, bool decode = true)
        {
            var reg = new Regex("<[^>]+>", RegexOptions.IgnoreCase);

            string stripped = reg.Replace(HTMLText, "");

            return decode ? HttpUtility.HtmlDecode(stripped) : stripped;
        }

        internal static string Truncate(this string value, int maxChars)
        {
            return value.Length <= maxChars ? value : value.Substring(0, maxChars - 3) + "...";
        }
    }
}
