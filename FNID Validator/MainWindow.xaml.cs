﻿using Extra.Utilities;
using System;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace FNID_Validator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string Suffix = "6759659904250490566427499489741A";

        public MainWindow()
        {
            FunctionName = new ObservableCollection<string>();
            FunctionName.Add(string.Empty);
            FunctionName.Add(string.Empty);

            InitializeComponent();

            //Conversion();
        }

        public static ObservableCollection<string> FunctionName { get; set; }

        private static void Conversion(string input)
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            string inputHex = BitConverter.ToString(inputBytes).Replace("-", "");

            string concatenated = inputHex + Suffix;

            byte[] hash = GetHash(concatenated);

            byte[] trimmedHash = new byte[4];

            Array.Copy(hash, 0, trimmedHash, 0, trimmedHash.Length);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(trimmedHash);

            FunctionName[1] = BitConverter.ToString(trimmedHash).Replace("-", "");
        }

        private static byte[] GetHash(string concatenated)
        {
            SHA1 sha1 = SHA1.Create();

            byte[] hash = MiscUtils.HexStringToByteArray(concatenated);

            return sha1.ComputeHash(hash);
        }

        private void InputTextChanged(object sender, TextChangedEventArgs e)
        {
            FunctionName[0] = (sender as TextBox).Text;

            Conversion(FunctionName[0]);
        }
    }
}
